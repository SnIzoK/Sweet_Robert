$scroll_to_top_button = $(".scroll-to-top-button")

setButtonPosition = (button, position)->
  if !button
    return

  bottom = position.bottom
  top = position.top
  min_top = position.min_top || 0
  max_top = position.max_top
  if typeof top == "function"
    top = top()
  else if top instanceof $
    top = top.height()
  if typeof min_top == 'function'
    min_top = min_top()
  else if min_top instanceof $
    min_top = min_top.height()

  if typeof max_top == 'function'
    max_top = max_top()
  else if max_top instanceof $
    max_top = max_top.height()




  scroll_top = $("body").scrollTop()
  if scroll_top > min_top
    computed_top = top
  else
    computed_top = min_top - scroll_top

  if top >= 0 && computed_top < top
    computed_top = top
  if !computed_top
    computed_top = 0

  increment = 0
  if position.increment
    if typeof position.increment == 'function'
      increment = position.increment()
    else
      increment = position.increment

    computed_top += increment


  button.css("top", computed_top)
  #console.log "top: ", computed_top


set_buttons_position = ()->
  setButtonPosition($scroll_to_top_button, {
    top: ()->
      window.innerHeight - 85

    min_top: ()->
      $(".abotu-us-banner").offset().top
    #max_top: ()->
    #  $(".footer-box").offset().top
  })

set_buttons_position()
$window.on "scroll resize", set_buttons_position

$("body").on "click touchstart", ".scroll-to-top-button", (e)->
  $link = $(this)
  href = $link.attr("href")
  TweenMax.to($window, 2,
    {
      scrollTo: {
        y: 0
        autoKill: true
      }
    }
    ease: Elastic.easeOut.config(1, 0.3)
    overwrite: 5
  )
  target_top = $(href).offset().top
  e.preventDefault()