$document.ready ->

$('.popup_button').on 'click', ->

  $('.popup-wrap').addClass('visible')
  $('body').addClass('popup-opened')
  index = $(this).closest('.one-product-box').parent().index()
  popup_slider.goToSlide(index)

close_slider_popup = ()->
  if $('.popup-slider-wrap').hasClass('visible')
    $('.popup-slider-wrap').removeClass('visible')
    $('body').removeClass('popup-opened')

$document.on "click", ".popup-slider-wrap .close_popup", close_slider_popup

$.clickOut(
  '.popup-slider-wrap .bx-wrapper'
  close_slider_popup
  {except: '.button, .prev-slide, .next-slide, .popup_button, .popup-order-wrap'}
)


$document.on 'click', '.order_form_button', ->
  product_id = $(this).closest(".one-product-box").attr("data-product-id")
  if !product_id
    console.log "not product id"
    $main_info_slider_container = $(this).closest(".info-slider")
    product_id = $main_info_slider_container.children().filter(".visible").attr("data-product-id")
    console.log "product_id: ", product_id


  $order_popup_wrap = $(".popup-order-wrap")

  $order_popup_wrap.find('.head-container').removeClass('hidden')
  $order_popup_wrap.find('.head-acept').removeClass('visible')
  $order_popup_wrap.find(".form-container").removeClass("sent")

  $order_popup_wrap.addClass('visible')

  $order_popup_wrap.attr("data-product-id", product_id)
  $order_popup_wrap.addClass('visible')
  $('.popup-slider-wrap').removeClass('visible')
  $('body').addClass('popup-opened')

close_order_popup = ()->
  if $('.popup-order-wrap').hasClass('visible')
    $('.popup-order-wrap').removeClass('visible')
    $('body').removeClass('popup-opened')

$document.on "click", ".popup-order-wrap .close_popup", close_order_popup

$.clickOut(
  '.popup-order-wrap .order-content'
  close_order_popup
  {except: '.button-form, .input-field, .order_form_button, .popup-wrap'}
)


$document.on 'submit', '.ajax-form', (e)->
  $form = $(this)

  e.preventDefault()

  return if !validate_form.call($form, true)


  data = {}
  url = $form.attr("data-url")

  if $form.hasClass("order-form")
    data = {
      order_request: {
        name: $form.find("input[name=name]").val()
        phone: $form.find("input[name=phone]").val()
        product_id: $form.closest(".popup-order-wrap").attr("data-product-id")
      }
    }
  else
    resource_name = $form.attr("data-resource-name")
    data[resource_name] = objectifyForm($form.serializeArray())

  $.ajax({
    data: data
    type: "post"
    url: $form.attr("data-url")
  })

  $form_container = $form.closest(".form-container")
  $form_container.addClass("sent")

  $form_container.find('.head-container').addClass('hidden')
  $form_container.find('.head-acept').addClass('visible')

  setTimeout (
    ->
      $form_container.find('.head-container').removeClass("hidden")
      $form_container.find('.head-acept').removeClass("visible")
    3000
  )

