messages = {
  inputs: {
    errors: {
      required: "Це поле обов'язкове"
    }
  }
}

window.init_form = ()->
  if !is_mobile_ua
    $(this).find(".input-phone input").mask("(999) 999 99 99", {})
  else
    $(this).find(".input-phone input").attr({"type": "tel", placeholder: "(0XX) XXX XX XX"})
$("form").each(init_form)

window.validate_form = (update_dom = false)->
  valid = true
  $form = $(this)
  $form.find(".input-field").each(
    ()->

      $input_wrap = $(this)

      valid_input = validate_input.call($input_wrap, update_dom)
      if valid
        valid = valid_input

      if !update_dom && !valid
        return false
      else
        return
  )
  console.log "valid_form: ", valid
  valid


window.validate_input = (update_dom = false)->
  $input_wrap = $(this)
  $input = $input_wrap.find("input, textarea")
  value = $input.val()
  required = $input_wrap.hasClass("required")
  valid = true
  console.log "input value: ", value
  if required && (!value || !value.length)
    valid = false
    if update_dom
      $input_wrap.addClass("invalid-required")
      message_text = messages.inputs.errors.required
      if !$input_wrap.find(".error-message-required").length
        $input_wrap.append("<span class='error-message error-message-required'>#{message_text}</span>")

  if valid
    $input_wrap.removeClass("invalid invalid-required").addClass("valid")
  else
    $input_wrap.removeClass("valid").addClass("invalid")

  valid



$document.on "keyup blur change", ".input-field", ()->
  validate_input.call($(this), true)
