#= require jquery
#= require jquery-ui
#= require jquery_ujs

#= require global

#     P L U G I N S

#= require plugins/jquery-easing
#= require plugins/jquery.appear
#= require plugins/clickout
#= require plugins/datepick
#= require plugins/form
#= require plugins/jquery.bxslider
#= require plugins/jquery.scrolldelta
#= require plugins/lightgallery.min
# require plugins/scroll-banner
#= require plugins/selectize.min
#= require plugins/parallax.min
#= require plugins/owl.carousel.min
#= require plugins/anchor_link

#= require plugins/split_text.min
#= require plugins/tween_max.min
#= require plugins/scroll_to_plugin.min
#= require plugins/delay
#= require plugins/objectify_form
#= require jquery.maskedinput/dist/jquery.maskedinput.min

#     I N I T I A L I Z E

#= require basic
#= require bxslider
#= require header
#= require forms
#= require popups
#= require tabs
#= require navigation
#= require bx
#= require owlCarousel
#= require scroll_to_top_button


# require post_initializer
