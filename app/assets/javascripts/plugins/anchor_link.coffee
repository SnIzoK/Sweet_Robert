$("body").on "click", "ul.nav-list li a", (e)->
  href = $link.attr("href")
  if is_iphone()
    e.preventDefault()
    target_top = $(href).offset().top
    $("body, html").animate(scrollTop: target_top)
    return false

  $link = $(this)

  TweenLite.to(window, 2, {scrollTo:"#{href}"});
  e.preventDefault()