$(document).ready(function(){
  $('.owl-carousel-ingredients').owlCarousel({
    center: true,
    items:6,
    loop:true,
    // startPosition: 2,
    dots:false,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 3
      },
      1200: {
        items: 5
      }
    }
  });

  $('.owl-carousel-testemonials').owlCarousel({
    items:1,
    dots:true,
    loop:true
  });

});

$('.btn-box .next-slide').click(function() {
    // console.log('works')
    var owl = $(this).closest('.testemonials').find('.owl-carousel')
    owl.trigger('next.owl.carousel');
})
$('.btn-box .prev-slide').click(function() {
    var owl = $(this).closest('.testemonials').find('.owl-carousel')
    owl.trigger('prev.owl.carousel', [300]);
})  




$('.info-ingredient .next-slide').click(function() {
    // console.log('works')
    var owl = $(this).closest('.ingredients-carousel-wrap').find('.owl-carousel')
    owl.trigger('next.owl.carousel');
})
$('.info-ingredient .prev-slide').click(function() {
    var owl = $(this).closest('.ingredients-carousel-wrap').find('.owl-carousel')
    owl.trigger('prev.owl.carousel', [300]);
})

var owl = $(".owl-carousel");
var $ingredients = $('.info-ingredient .description')
$document.on('changed.owl.carousel', ".owl-carousel-ingredients",function(e){
    setTimeout(
        function(){
            var data_index = owl.find(".owl-item.active.center .ingredient").attr("data-index")

            $ingredients.removeClass("active")
            var $description_block = $ingredients.eq(data_index)
            if (!$description_block.length)
                $description_block = $('.info-ingredient .description').eq(0)
            $description_block.addClass('active')
        },
        0
    )
})




// $('.info-ingredient .next-slide').click(function() {
//     // console.log('works')
//     var owl = $(this).closest('.ingredients-carousel-wrap').find('.owl-carousel')
//     owl.trigger('next.owl.carousel');
// })
// $('.info-ingredient .prev-slide').click(function() {
//     var owl = $(this).closest('.ingredients-carousel-wrap').find('.owl-carousel')
//     owl.trigger('prev.owl.carousel', [300]);
// })