$(document).ready ->

    #     M A I N     B A N N E R     S L I D E R

    banner_slider = $('.main-banner-slider').bxSlider
        controls: false
        pagerCustom: '#bx-pager-baner'
        infiniteLoop: true
        speed: 750
        pause: 3000
        auto: true
        mode: 'fade'
        onSlideBefore: ($slideElement, oldIndex, newIndex)->
            current = banner_slider.getCurrentSlide()
            $('.info-slider .content-slider').removeClass('visible')
            $('.info-slider .content-slider').eq(current).addClass('visible')
            $('#bx-pager-baner .bx-pager-item a').removeClass('active')
            $('#bx-pager-baner .bx-pager-item a').eq(current).addClass('active')
    

    #      P O P U P      S L I D E R

    window.popup_slider = $('.popup-slider').bxSlider
        controls: false
        pager: false
        mode: 'fade'
    $('.popup-wrap .prev-slide').click ->
        popup_slider.goToPrevSlide()
    $('.popup-wrap .next-slide').click ->
        popup_slider.goToNextSlide()
