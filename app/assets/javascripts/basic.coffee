customScroll = (event) ->
  if $('.showPopup').length
    return true

  delta = 0
  if !event
    event = window.event
  if event.wheelDelta
    delta = event.wheelDelta / 120
  else if event.detail
    delta = -event.detail / 3
  else if event.originalEvent && event.originalEvent.deltaY
    delta = -event.originalEvent.deltaY

  if delta
    scrollTop = $window.scrollTop()
    #increment = -1 * (parseInt(delta * 100) * 4)
    step = window.innerHeight * 0.5
    increment = if delta < 0 then step else step * -1
    finScroll = scrollTop + increment

    TweenMax.to($window, 1.5,
      {
        scrollTo: {
          y: finScroll
          autoKill: true
        }
      }
      ease: Elastic.easeOut.config(1, 0.3)
      overwrite: 5
    )
  if event.preventDefault
    event.preventDefault()
  event.returnValue = false
  #console.log "customScroll: e: ", event


scroll_delay = ()->
  delay("custom_scroll", customScroll, 20, true, false)

$document.on "mousewheel", scroll_delay
if $document.addEventListener
  document.addEventListener('DOMMouseScroll', customScroll, false)

$appearable_elements = $(".appear, .appear-tree *")
$appearable_elements.appear()



appear_handler = (event, $all_appeared_elements)->
  $all_appeared_elements.each(
    ()->
      $e = $(this)
      appeared = $e.data("_appeared")
      if !appeared
        $e.data("_appeared", true)
        $e.trigger("appear_once", event, $all_appeared_elements, $e)
        $e.addClass("appeared")
        $tree_root = $e.closest(".appear-tree")
        $tree_root.trigger("appear:tree:node", event, $all_appeared_elements, $e)

  )

$document.on "appear", "*", appear_handler
#appear_handler.call(this, {}, $appearable_elements.filter(":appeared"))

page_loaded_handler = ()->
  # $('body').addClass('loaded');
  setTimeout ()->
    $('#loader-wrapper').fadeOut(
      complete: ()->
        $.force_appear()
        $window.trigger("after_preloader")
    )
  , 2000


window.addEventListener('load', page_loaded_handler)

$document.on "appear_once", ".fadeInUp-tree *, .fadeInUp", ->
  $e = $(this)
  t = new TimelineLite
  t.fromTo($e, 0.5, {opacity: 0, y: "20%"}, {opacity: 1, y: "0%"})

$document.on "appear_once", ".one-product-box", ->
  $e = $(this)

  t = new TimelineLite
  t.delay(0.5)
  t.fromTo($e, 0.5, {opacity: 0, y: "20%"}, {opacity: 1, y: "0%"})



animateText = (jquery_element_or_selector, letter_duration, letter_delay, timeline)->
  $e = $(jquery_element_or_selector)
  timeline ?= new TimelineLite
  letter_duration ?= 0.4
  letter_delay ?= 0.04

  mySplitText = new SplitText($e, {type:"words,chars"})
  chars = mySplitText.chars

  timeline.staggerFrom(chars, letter_duration, {opacity:0,  ease:Back.easeOut}, letter_delay, "+=0");

  timeline


$document.on "appear_once", ".article-title", ->
  $e = $(this)
  animateText($e)
