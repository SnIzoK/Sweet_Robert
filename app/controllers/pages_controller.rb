class PagesController < ApplicationController
  caches_page :index

  def index
    @products = Product.published
    @featured_products = @products.featured
    @ingredients = Ingredient.published
    @testimonials = Testimonial.published
    @show_products_popup = true
    set_page_metadata(:home)
  end

end