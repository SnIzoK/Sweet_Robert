class Product < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name, :description, :weight_description, :expiration_period_description

  boolean_scope :published
  boolean_scope :featured
  scope :order_by_sorting_position, -> { order("sorting_position asc") }

  default_scope do
    order_by_sorting_position
  end

  has_cache do
    pages :home
  end

  image :avatar, styles: {small: "400x400#"}
  image :large_image, styles: {large: "1200x750#"}
  image :featured_image, styles: {large: "1920x1248#"}
end
