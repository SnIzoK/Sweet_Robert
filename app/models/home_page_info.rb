class HomePageInfo < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :all

  has_cache do
    pages :home
  end
end
