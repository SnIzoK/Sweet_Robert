class OrderRequest < ActiveRecord::Base
  attr_accessible *attribute_names

  include Cms::Notifier

  belongs_to :product

  validates_presence_of :name, :phone

  extend Cms::FormAttributesHelper

  def self.fields_from_model
    self.form_attributes(true)
  end
end
