class Testimonial < ActiveRecord::Base
  attr_accessible *attribute_names

  globalize :name, :short_description

  boolean_scope :published
  scope :order_by_sorting_position, -> { order("sorting_position asc") }

  default_scope do
    order_by_sorting_position
  end

  has_cache do
    pages :home
  end

  image :avatar, styles: {small: "200x200#"}
end
