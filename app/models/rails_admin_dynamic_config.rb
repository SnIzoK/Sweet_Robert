module RailsAdminDynamicConfig
  class << self
    def configure_rails_admin(initial = true)
      RailsAdmin.config do |config|

        ### Popular gems integration

        ## == Devise ==
        config.authenticate_with do
          warden.authenticate! scope: :user
        end
        config.current_user_method(&:current_user)

        ## == Cancan ==
        #config.authorize_with :cancan

        ## == Pundit ==
        # config.authorize_with :pundit

        ## == PaperTrail ==
        # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

        ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration



        if initial
          config.actions do
            dashboard                     # mandatory
            index                         # mandatory
            new do
              #except [CallRequest, ConsultationRequest, MeterRequest, ContactsRequest, PartnershipRequest]
            end
            export
            bulk_delete
            show
            edit
            delete
            show_in_app
            props do
              only []
            end
            #edit_model
            nestable do
              only [Cms::Page, Product, Ingredient]
            end

            ## With an audit adapter, you can add:
            # history_index
            # history_show
          end
        end

        config.navigation_labels do
          {
            feedbacks: 100,
            home: 200,
            about_us: 300,
            projects: 400,
            services: 500,
            blog: 600,
            faq: 700,
            contacts: 800,
            vacancies: 900,
            tags: 1000,
            users: 1100,
            settings: 1200,
            pages: 1300,
            assets: 1400
          }
        end

        config.navigation_static_links = {
           mailchimp: "/admin/mailchimp",
           locales: "/file_editor/locales",
           site_data: "/file_editor/site_data.yml"
        }

        #config.include_models Attachable::Asset


        #
        #
        config.include_models Cms::SitemapElement, Cms::MetaTags
        config.include_models Cms::Page
        config.model Cms::Page do
          navigation_label_key(:pages, 1)
          nestable_list({position_field: :sorting_position, scope: :order_by_sorting_position})
          object_label_method do
            :custom_name
            #{
            #k = @bindings[:object].type.underscore.split("/").last
            #I18n.t("activerecord.models.pages.#{k}", raise: true) rescue k.humanize
            #}
          end
          list do
            sort_by do
              "sorting_position"
            end

            field :name do
              def value
                k = @bindings[:object].type.underscore.split("/").last
                I18n.t("activerecord.models.pages.#{k}", raise: true) rescue k.humanize
              end
            end

            field :h1_text do
              def value
                @bindings[:object].h1_text
              end
            end
          end

          edit do
            field :name do
              read_only true
              def value
                k = @bindings[:object].type.underscore.split("/").last
                I18n.t("activerecord.models.pages.#{k}", raise: true) rescue k.humanize
              end
            end
            field :translations, :globalize_tabs
            field :seo_tags

          end

        end

        config.model_translation Cms::Page do
          field :locale, :hidden
          field :h1_text
        end


        config.model Cms::MetaTags do
          visible false
          field :translations, :globalize_tabs
        end

        config.model_translation Cms::MetaTags do
          field :locale, :hidden
          field :title
          field :keywords
          field :description
        end


        config.model Cms::SitemapElement do
          visible false

          field :display_on_sitemap
          field :changefreq
          field :priority
        end

        # config.include_models Attachable::Asset
        #
        # config.model Attachable::Asset do
        #   navigation_label_key(:assets, 1)
        #   field :data
        #   watermark_position_field(:data)
        #   field :sorting_position
        #   field :translations, :globalize_tabs
        # end
        #
        # config.model_translation Attachable::Asset do
        #   field :locale, :hidden
        #   field :data_alt
        #   field :youtube_video_id
        #   field :vimeo_video_id
        # end


        config.include_models User
        config.model User do
          navigation_label_key(:users, 1)
          field :email
          field :password
          field :password_confirmation
        end

        config.include_models Cms::Tag, Cms::Tagging

        config.model Cms::Tag do
          navigation_label_key(:tags, 1)
          field :translations, :globalize_tabs
          field :blog_articles
        end

        config.model_translation Cms::Tag do
          field :locale, :hidden
          field :name
          field :url_fragment do
            help do
              I18n.t("admin.help.#{name}")
            end
          end
        end

        config.model Cms::Tagging do
          visible false
        end

        # ===================================================
        # Requests
        # ===================================================
        config.configure_forms(ContactRequest, OrderRequest)

        # ===================================================
        # Application specific models
        # ===================================================
        config.include_models Product, Ingredient, Testimonial


        config.model Product do
          nestable_list({position_field: :sorting_position})
          navigation_label_key :home, 2

          edit do
            field :published
            field :featured
            field :translations, :globalize_tabs
            field :avatar
            field :large_image
            field :featured_image
          end

          list do
            field :published
            field :featured
            translated_field :name
            field :avatar
          end
        end

        config.model_translation Product do
          field :locale, :hidden
          field :name
          field :description, :ck_editor
	  field :weight_description
	  field :expiration_period_description
        end

        config.model Ingredient do
          nestable_list({position_field: :sorting_position})
          navigation_label_key :home, 3

          edit do
            field :published
            field :translations, :globalize_tabs
            field :image
          end

          list do
            field :published
            translated_field :name
            field :image
          end
        end

        config.model_translation Ingredient do
          field :locale, :hidden
          field :name
          field :short_description
        end

        config.model Testimonial do
          nestable_list({position_field: :sorting_position})
          navigation_label_key :home, 3

          edit do
            field :published
            field :translations, :globalize_tabs
            field :avatar
          end

          list do
            field :published
            translated_field :name
            translated_field :short_description
            field :avatar
          end
        end

        config.model_translation Testimonial do
          field :locale, :hidden
          field :name
          field :short_description
        end
        
        config.model HomePageInfo do
          navigation_label_key :home, 4
          field :translations, :globalize_tabs
        end
        
        config.model_translation HomePageInfo do
          field :locale, :hidden
          group :home do
            field :about_title
            field :about_text, :ck_editor
            field :principles_title
            field :principles_text, :ck_editor
            field :mission_title
            field :mission_description, :ck_editor
            field :products_title
            field :products_text, :ck_editor
            field :ingredients_title
            field :ingredients_text, :ck_editor
            field :order_block_title
            field :order_block_text, :ck_editor
            field :clients_title
            field :contact_form_title
            field :contact_form_text, :ck_editor
          end
          
          group :footer do
            field :footer_about_short_description, :ck_editor
            field :footer_developed_by
            field :footer_copyright
          end
          
          group :popups do
            field :order_form_title
            field :order_form_text, :ck_editor
          end
          
          group :forms do
            field :input_name_label
            field :input_phone_label
            field :input_comment_label
            field :submit_button
            field :form_success_title
            field :form_success_text, :ck_editor
          end

          group :product_parameters do
            field :product_weight
            field :product_expiration_period
          end
          
          group :misc do
            field :order_button
            field :menu_about_us
            field :menu_products
            field :menu_contacts

            field :scroll_up_html_title
            field :close_button_html_title
            field :social_facebook_html_title
            field :social_instagram_html_title
            field :map_link_html_title
            field :email_link_html_title
          end
        end
      end
    end
  end
end
