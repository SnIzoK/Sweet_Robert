module ApplicationHelper
  def devise_controller?
    params[:controller].start_with?("users/")
  end

  def google_site_verification
    if Rails.env.production?
      code = ENV["GOOGLE_SITE_VERIICATION_CODE"]
      if code.present?
        "<meta name=\"google-site-verification\" content=\"#{code}\"/>".html_safe
      end
    end
  end

  def text(key)
    @home_page_info ||= HomePageInfo.first_or_initialize
    s = @home_page_info.send(key).try(:html_safe)
    s.present? ? s : key.to_s
  end

  def footer_copyright_text
    start_year = 2017
    current_year = Date.today.year
    years = "#{start_year}"
    if current_year > start_year
      years += " &mdash; #{current_year}"
    end
    text(:footer_copyright).gsub(/\%\{years\}/, years).html_safe
  end

  def label_text(key, options = {})
    name = text("input_#{key}_label")
    if options[:required]
      name += "*"
    end

    name
  end

  def html_title(key)
    text("#{key}_html_title")
  end
end
