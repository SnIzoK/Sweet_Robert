require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Name
  class Application < Rails::Application

    # Do not swallow errors in after_commit/after_rollback callbacks.
    config.active_record.raise_in_transactional_callbacks = true

    config.time_zone = 'Kyiv'
    config.active_record.default_timezone = :local

    config.i18n.default_locale = :uk
    config.i18n.available_locales = [:ru, :uk]

    # ckeditor
    Rails.application.config.assets.precompile += %w(ckeditor/* ckeditor/lang/*)
    config.assets.enabled = true
    config.assets.precompile += Ckeditor.assets
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)

    # file_editor
    config.assets.precompile += %w(fonts/octicons/octicons.woff cms/file_editor.css cms/file_editor.js)
  end
end
