Rails.application.routes.draw do
  root to: "pages#index"

  devise_for :users, module: "users", path: "", path_names: {
      sign_in: "login",
      sign_out: 'logout',
  }

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount Cms::Engine => '/'
  mount Ckeditor::Engine => '/ckeditor'

  controller :forms do
    post "contact_request"
    post "order_request"
  end

  match "*url", to: "application#render_not_found", via: :all
end