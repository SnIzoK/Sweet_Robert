class CreateTestimonials < ActiveRecord::Migration
  def change
    create_table :testimonials do |t|
      t.boolean :published
      t.integer :sorting_position

      t.string :name
      t.string :short_description
      t.attachment :avatar

      t.timestamps null: false
    end

    create_translation_table(Testimonial, :name, :short_description)
  end
end
