class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.boolean :published
      t.integer :sorting_position
      t.string :name
      t.string :description
      t.attachment :avatar
      t.attachment :large_image

      t.timestamps null: false
    end

    create_translation_table(Product, :name, :description)
  end
end
