class CreateHomePageInfos < ActiveRecord::Migration
  def change
    create_table :home_page_infos do |t|
      t.string :about_title
      t.text :about_text
      t.string :principles_title
      t.text :principles_text
      t.string :mission_title
      t.text :mission_description
      t.string :products_title
      t.text :products_text
      t.string :ingredients_title
      t.text :ingredients_text
      t.string :order_block_title
      t.text :order_block_text
      t.string :clients_title
      t.text :clients_text
      t.string :contact_form_title
      t.text :contact_form_text

      t.text :footer_about_short_description
      t.text :footer_developed_by
      t.text :footer_copyright
      t.string :order_form_title
      t.text :order_form_text
      t.string :order_button
      t.string :submit_button
      t.string :menu_about_us
      t.string :menu_products
      t.string :menu_contacts

      t.string :scroll_up_html_title
      t.string :close_button_html_title
      t.string :social_facebook_html_title
      t.string :social_instagram_html_title
      t.string :map_link_html_title
      t.string :email_link_html_title

      t.string :input_name_label
      t.string :input_phone_label
      t.string :input_comment_label


      t.timestamps null: false
    end

    create_translation_table(HomePageInfo, :all)
  end
end
