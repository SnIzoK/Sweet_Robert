class CreateIngredients < ActiveRecord::Migration
  def change
    create_table :ingredients do |t|
      t.boolean :published
      t.integer :sorting_position
      t.string :name
      t.text :short_description
      t.attachment :image

      t.timestamps null: false
    end

    create_translation_table(Ingredient, :name, :short_description)
  end
end
