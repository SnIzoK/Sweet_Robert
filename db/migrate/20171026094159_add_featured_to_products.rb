class AddFeaturedToProducts < ActiveRecord::Migration
  def change
    change_table :products do |t|
      t.boolean :featured
      t.attachment :featured_image
    end
  end
end
