class AddProductParametersToHomePageInfos < ActiveRecord::Migration
  def change
    [:home_page_infos, :home_page_info_translations].each do |table_name|
      change_table table_name do |t|
        t.string :product_weight
        t.string :product_expiration_period
      end
    end
  end
end
