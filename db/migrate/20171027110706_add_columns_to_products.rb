class AddColumnsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :weight_description, :string
    add_column :products, :expiration_period_description, :string

    add_column :product_translations, :weight_description, :string
    add_column :product_translations, :expiration_period_description, :string

  end
end
