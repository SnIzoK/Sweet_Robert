class AddColumsToHomePageInfos < ActiveRecord::Migration
  def change
    [:home_page_infos, :home_page_info_translations].each do |table_name|
      change_table table_name do |t|
        t.string :form_success_title
        t.text :form_success_text
      end
    end
  end
end
