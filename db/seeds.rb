# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

products_collection = [
    {
        title: "Сирник",
        description: "<p>Чотири тонесеньких коржі  поєднанні топленого шоколаду та рому. Ласувати нашими cмаколиками за вжди смачною </p>",
        avatar: 'products/small/syrnyk.jpg',
        popup_avatar: "products/big/01_syrnyk_big.jpg"
    },
    {
        title: "Горіховий з персиками",
        description: "<p>Два бісквітно-горіхових та один білково-кокосовий коржі, поєднані легким вершково-сметанковим кремом та щедрим прошарком соковитих персиків, що зберігають усі свої корисні властивості навіть, коли у тортику.</p><p>Легкий як хмаринка.
Смак, що запам’ятовується!</p>",
        avatar: "products/small/02_gorihoviy_z_persykamy.jpg",
        popup_avatar: "products/big/02_gorihoviy_z_persykamy_big.jpg"
    },
    {
        title: "Цісар",
        description: "<p>Чотири неймовірно-легких коржі: горіховий з волоськими горішками, маковий корж, з родзинками та шоколадний, які поєднуються вершково-кавовим кремом на основі добірного коньяку або ж нашого фірмового ванільного сиропу.</p> <p>Впевнені, він неодмінно завоює
ваш витончений смак!</p>",
        avatar: "products/small/03_cisar.jpg",
        popup_avatar: "products/big/03_cisar_big.jpg"
    },
    {
        title: "Шоколадно горіховий",
        description: "<p>Три бісквітно-білкових какао коржі, поверх яких волоські горіхи та шматочки справжнього чорного шоколаду, поєднані вишукано-ніжним вершковим кремом на основі натурального згущеного молока.</p> <p>Вдалий вибір для особливого задоволення!</p>",
        avatar: "products/small/04_shokoladno_horihoviy.jpg",
        popup_avatar: "products/big/04_shokoladno_horihoviy_big.jpg"
    },
    {
        title: "Вишні на хмарах",
        description: "<p>Тонке листкове тісто, перекладене ніжним сметанковим кремом у поєднанні з соковитими вишнями та карамелізованим маком. Вишні печуться на білковому суфле.</p> <p>Смачно з любов’ю!</p>",
        avatar: "products/small/05_vishny_na_hmarah.jpg",
        popup_avatar: "products/big/05_vishny_na_hmarah_big.jpg"
    },
    {
        title: "Медова пані",
        description: "<p>Вісім тонесеньких медових коржі та легкий заварний крем. Тортик политий чорним шоколадом.</p> <p>Для шанувальників смачно та солодко проводити час!</p>",
        avatar: "products/small/06_medova_pani.jpg",
        popup_avatar: "products/big/06_medova_pani_big.jpg"
    },
    {
        title: "Ябулчний з корицею",
        description: "<p>Три п’ янких коржі: двоє наоснові волоських горіхів, третій - бісквітний. Кожен з коржів промочений запашною львівською кавою та коньяком, або ж нашим фірмовим ванільним сиропом. Коржі поєднані легким вершковим кремом, на який викладаються карамелізовані яблука з додаванням ароматної кориці.</p> <p>Смачніше не буває!</p>",
        avatar: "products/small/07_yabluchniy_z_koryceyu.jpg",
        popup_avatar: "products/big/07_yabluchniy_z_koryceyu_big.jpg"
    },
    {
        title: "Сирково-ягідний",
        description: "<p>Три тонесньких, бісквітних коржі просочених спеціальним сиропом із пахучої кави та коньяку, у поєднанні з вершковим крем- сиром; Поверх тортика – запашні, стиглі чорниці, залиті нашим фірмовим желе, власноруч звареним лише з добірних, cоковитих вишень.</p> <p>Смачно та вишукано, насолоджуйтесь!</p>",
        avatar: "products/small/08_sirkovo_yabluchniy.jpg",
        popup_avatar: "products/big/08_sirkovo_yabluchniy_big.jpg"
    },
    {
        title: "Горіхово-маковий з меренгою",
        description: "<p>Тортик складається з трьох пухкесеньких коржів: горіхового (на основі волоських горішків), макового та неймовірно-легкого коржа з французькоі меренги, поєднані між собою найніжнішим вершково-кавовим кремом.</p> <p></p>",
        avatar: "products/small/09_gorihovo_makoviy_z_merengoyu.jpg",
        popup_avatar: "products/big/09_gorihovo_makoviy_z_merengoyu_big.jpg"
    },
    {
        title: "Вишня в кришталі",
        description: "<p>Два темні коржі на основі чорного шоколаду та ніжний бісквіт, просочений ароматною львівською кавою та коньяком або ж нашим фірмовим ванільним сиропом. Легкий вершково- ванільний крем та соковиті вишні у п’янкому вишневому желе власного приготування – є чудовим свідченням виключно ідеального смаку!</p>",
        avatar: "products/small/10_vyshnya_v_kryshtali.jpg",
        popup_avatar: "products/big/10_vyshnya_v_kryshtali_big.jpg"
    },
    {
        title: "Вишня в шоколаді",
        description: "<p>Два бісквітно-шоколадні коржі та ніжний бісквіт, просочений ароматною кавою та коньяком. Поєднується вишуканим кремом та топленим, чорним шоколадом.</p>",
        avatar: "products/small/11_vyshnya_v_shokoladi.jpg",
        popup_avatar: "products/big/11_vyshnya_v_shokoladi_big.jpg"
    },
    {
        title: "Шоколадна спокуса",
        description: "<p>Чотири тонесеньких коржі: два бісквітно-шоколадні та два білкові зі шматочками натурального чорного шоколаду та волоськими горішками. Ніжний вершково- ванільний крем у поєднанні топленого шоколаду та рому.</p> <p>Ласувати нашими cмаколиками
завжди смачно!</p>",
        avatar: "products/small/12_shokoladna_spokusa.jpg",
        popup_avatar: "products/big/12_shokoladna_spokusa_big.jpg"
    },
    {
        title: "Львівський з вишнями",
        description: "<p>П’ять легких, тонесеньких коржів у поєднанні заварного крему. Кожен з коржів перешарований соковитими вишнями у власному соці.</p> <p>Шанувальникам вишневих забаганок!</p>",
        avatar: "products/small/13_lvivskiy_z_vyshnyamy.jpg",
        popup_avatar: "products/big/13_lvivskiy_z_vyshnyamy_big.jpg"
    },
    {
        title: "Небесні ласощі",
        description: "<p>Тоненький хрумкий корж, поверх якого ніжні карамелізовані яблука у нашому фірмовому лимонному желе та горіховий бісквіт, просочений ароматною кавою та коньяком або спеціальним ванільним сиропом у поєднанні ніжного вершково-горіхового крему.</p> <p>На висоті завжди смачніше!</p>",
        avatar: "products/small/14_nebesni_lasoshchi.jpg",
        popup_avatar: "products/big/14_nebesni_lasoshchi_big.jpg"
    },
    {
        title: "Сирник з ябулками",
        description: "<p>Ніжна печена маса, на основі ванільно-лимонного крем-сиру та родзинок. Прикрашається шматочками карамелізованих яблук під нашим фірмовим апельсиновим желе.</p> <p>Солодкі мрії завжди здійснюються!</p>",
        avatar: "products/small/15_syrnyk_z_yablukamy.jpg",
        popup_avatar: "products/big/15_syrnyk_z_yablukamy_big.jpg"
    },
    {
        title: "Снікерс від Роберта",
        description: "<p>Два пухких шоколадних коржі, поміж якими смачнезна маса з арахісових горішків, солодких хрумких пластівців та ніжного вершкового крему на основі натурального згущеного молока.</p> <p>Ось воно дитяче щастя!</p>",
        avatar: "products/small/16_snikers_vid_roberta.jpg",
        popup_avatar: "products/big/16_snikers_vid_roberta_big.jpg"
    },
    {
        title: "Сирник маковий",
        description: "<p>Ніжна печена маса на основі ванільно-лимонного крем-сиру та родзинок з пухкою маковою начинкою посередині. Cирник политий чорним шоколадом.</p> <p>Для справжніх поціновувачів львівських традицій!</p>",
        avatar: "products/small/17_sirnyk_makoviy.jpg",
        popup_avatar: "products/big/17_sirnyk_makoviy_big.jpg"
    },
    {
        title: "Горіхова насолода",
        description: "<p>П’ять коржів: чотири повітряні листкові коржі з арахісом або ж волоськими горішками у ніжному білковому суфле та маковий бісквіт посередині, з’єднані легким вершковим кремом.</p> <p>Добрий смаколик для високої мети!</p>",
        avatar: "products/small/18_horihova_nasoloda.jpg",
        popup_avatar: "products/big/18_horihova_nasoloda_big.jpg"
    },
    {
        title: "Яблучно горіховий кекс",
        description: "<p>Вишуканий кекс з волоськими горішками, свіжими карамелізованими яблуками, родзинками та ароматною корицею.</p>",
        avatar: "products/small/19_yabluchno_horihoviy_kes.jpg",
        popup_avatar: "products/big/19_yabluchno_horihoviy_kes_big.jpg"
    },
    {
        title: "Апельсиновий",
        description: "<p>Родзинки, сушений абрикос та шматочки свіжого апельсину чи інших сезонних ягід, в поєднанні з пухким бісквітом.</p> <p>Тоненька горіхова меренга є найкращим довершенням його неземного смаку.</p>",
        avatar: "products/small/20_apelsinoviy.jpg",
        popup_avatar: "products/big/20_apelsinoviy_big.jpg"
    },
    {
        title: "Ябулка в карамелі",
        description: "<p>П’ятеро унікально-ніжних, тонесеньких коржів, перекладені карамелізованими яблуками та заварним кремом.</p> <p>Для любителів поласувати чимось смачненьким!</p>",
        avatar: "products/small/21_yabluka_u_karameli.jpg",
        popup_avatar: "products/big/21_yabluka_u_karameli_big.jpg"
    },
    {
        title: "Заварні тістечка",
        description: "<p>Неймовірно легке заварне тісто з додаванням ніжного заварного крему.</p>",
        avatar: "products/small/22_zavarni_tistechka.jpg",
        popup_avatar: "products/big/22_zavarni_tistechka_big.jpg"
    },
    {
        title: "Смакота",
        description: "<p>П’ятеро тонких амонякових коржі перекладені ніжним заварним кремом у поєднанні з вишнями та маком.</p>",
        avatar: "products/small/23_smakota.jpg",
        popup_avatar: "products/big/23_smakota_big.jpg"
    },
]

products_collection.each do |product_data|
  p = Product.create
  p.translations.create(locale: :uk, name: product_data[:title], description: product_data[:description])
  p.avatar = File.open(Rails.root.join("app/assets/images", product_data[:avatar]))
  p.large_image = File.open(Rails.root.join("app/assets/images", product_data[:popup_avatar]))
  p.published = true
  p.save
end

ingredients = [
    {
        image: "img/slider-1.jpg",
        name: "Молоко",
        short_description: "Ми використовуємо молоко із святих коров індії яких годують виключно добірною травою."
    },
    {
        image: "img/slider-2.jpg",
        name: "Фісташки",
        short_description: "Ми використовуємо молоко із святих коров індії яких годують виключно добірною травою."
    },
    {
        image: "img/slider-3.jpg",
        name: "Яйця",
        short_description: "Ми використовуємо молоко із святих коров індії яких годують виключно добірною травою."
    },
    {
        image: "img/slider-4.jpg",
        name: "Ягоди",
        short_description: "Ми використовуємо молоко із святих коров індії яких годують виключно добірною травою."
    },
    {
        image: "img/slider-5.jpg",
        name: "Фрукти",
        short_description: "Ми використовуємо молоко із святих коров індії яких годують виключно добірною травою."
    },
    {
        image: "img/slider-6.jpg",
        name: "Борошно",
        short_description: "Ми використовуємо молоко із святих коров індії яких годують виключно добірною травою."
    }
]

ingredients.each do |ingredient_data|
  i = Ingredient.create
  i.translations.create(locale: :uk, name: ingredient_data[:title], short_description: ingredient_data[:short_description])
  i.image = File.open(Rails.root.join("app/assets/images", ingredient_data[:image]))
  i.published = true
  i.save
end

testimonials = [
    {
        name: "Ivan Ogirko",
        image: "img/testimonials.jpg",
        short_description: "Вже давно відомо, що читабельний зміст буде заважати зосередитись людині, яка оцінює композицію сторінки. Сенс використання Lorem Ipsum полягає в тому, що цей текст має більш-менш нормальне розподілення літер на відміну від, наприклад, Тут іде текст. Тут іде текст."
    }
]

testimonials.each do |testimonial_data|
  t = Testimonial.create
  t.translations.create(locale: :uk, name: testimonial_data[:title], short_description: testimonial_data[:short_description])
  t.avatar = File.open(Rails.root.join("app/assets/images", testimonial_data[:image]))
  t.published = true
  t.save
end